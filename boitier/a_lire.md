Le boîtier est modélisé à l'aide du logiciel de conception assistée par ordinateur (CAO) libre FreeCAD.

https://www.freecad.org

La modélisation est lié à une feuille de calcul dynamique (intégrée dans le même fichier .FCStd) indiquant les côtes du boîtier et des composants électronique à intégrer. Cela permet à FreeCAD de modifier la pièce en temps réel en fonction des éditions des dimensions.

https://wiki.freecad.org/Manual:Using_spreadsheets/fr