% Programmer l'Arduino

# Ressources

> La liste des outils matériels et logiciels utilisés dans ce guide.

**Matériels**

* Arduino Uno Rev3 : https://store.arduino.cc/products/arduino-uno-rev3

**Logiciels**

* (x)ubuntu : https://ubuntu.com/
* Flatpak : https://flatpak.org/setup/Ubuntu
* Arduino IDE v2 : https://flathub.org/apps/details/cc.arduino.IDE2

# Méthodologie

## Installer Arduino IDE v2 sur (x)ubuntu

**Installer Flatpak**

> Depuis le terminal

`sudo apt install flatpak -y`

![Commande d'installation du logiciel Flatpak](img/install_flatpak.png "Commande d'installation du logiciel Flatpak")

**Installer Arduino IDE v2**

> Depuis le terminal

`flatpak install flathub cc.arduino.IDE2`

Accès ports séries

> Depuis le terminal, taper ces commandes pour donner accès à Arduino IDE v2 aux ports séries de x(ubuntu) afin de pouvoir téléverser le programme.

`sudo usermod -a -G tty $USER`[^1]  
`sudo usermod -a -G dialout $USER`

[^1]: `$USER` est une variable pour indiquer l'utilisateur courant de la session sur l'ordinateur.

## Configurer Arduino IDE v2

**Mettre en français**

* Démarrer le logiciel Arduino IDE v2,
* dans la barre de menu, sélectionner `File`, `Preferences...`,
* dans le menu déroulant `Language:`, sélectionner `français`,
* `OK`.

![Mettre Arduino IDE en Français](img/ide_francais.png "Mettre Arduino IDE en Français")

**Installer le pilote pour la carte Uno**

* Dans la barre de menu, sélectionner `Outils`, `Carte`, `Gestionnaire de carte...`
* `INSTALLER` `Arduino AVR Boards`.

![Installation du pilote de la carte Arduino Uno](img/install_pilote.png "Installation du pilote de la carte Arduino Uno")

**Sélectionner la carte Uno**

* Dans la barre de menu, sélectionner `Outils`, `Carte`, `Arduino AVR Boards`, `Arduino Uno`[^2].

**Installer les bibliothèques**

> Les bibliothèques seront téléversé avec le programme compilé dans l'Arduino. Il faut déjà ouvrir le programme.

* Dans la barre de menu, sélectionner `Outils`, `Gérer les bibliothèques...`,
* Écrire dans la barre de recherche `Ultrasonic`,
* `INSTALLER` `Ultrasonic par Erick Simoes`.
* Écrire dans la barre de recherche `RTClib`,
* `INSTALLER` `RTClib par Adafruit`.[^3]
* Écrire dans la barre de recherche `Seeed FS`,
* `INSTALLER` `Seeed Arduino FS" par Hongtai.liu`.[^4]

[^2]: Carte `Arduino Uno` simple, sans `WiFi` ou `Mini`.
[^3]: Sera installer par la même occasion la bibliothèque `Adafruit BusIO`.
[^4]: Pour le type "File".

## Charger le programme

> Cette étape consiste à charger dans votre logiciel Arduino IDE v2 le programme qui sera dans un second temps téléversé dans votre Arduino Uno.

* Enregistrer le fichier *prog-capteur_etude-distance-velo_release.ino*[^5] dans votre ordinateur.
* Charger le fichier dans votre projet/croquis ouvert sur le logiciel Arduino IDE v2, dans la barre de menu, sélectionner `Fichier`, `Ouvrir...`, sélectionner le fichier téléchargé préalablement dans l'ordinateur.
* Arduino IDE v2 indique vouloir déplacer le fichier dans le dossier contenant le projet/croquis homonyme, sélectionner `Ok`.

![Arduino IDE demande le déplacement du fichier](img/deplacement_fichier.png "Arduino IDE demande le déplacement du fichier")

* Une nouvelle fenêtre s'ouvre et affiche le fichier téléchargé, contenant le programme à téléverser sur l'Arduino Uno. Fermer l'ancienne fenêtre qui devient inutile.

[^5]: Le nom exact du fichier n'est pas définit lors de la rédaction de ce guide.

## Compiler et téléverser le programme

> Compiler transforme le fichier contenant du code en un programme exploitable par l'Arduino. Téléverser envoi le programme dans l'Arduino.

* Connecter en USB l'ordinateur et l'Arduino.

![Connexion entre ordinateur et Arduino via USB](img/connnecter_ordi_arduino.jpg "Connexion entre ordinateur et Arduino via USB")

* Dans la barre de menu, sélectionner `Croquis`, `Vérifier / Compiler`,

![Compiler le programme](img/compilation_programme.png "Compiler le programme")

* dans la barre de menu, sélectionner `Croquis`, `Téléverser`.
* Débrancher l'ordinateur de l'Arduino.

V 0.3 - 18 décembre 2022  
CC-BY-SA Laboratoire Sauvage - contact@laboratoire-sauvage.fr
