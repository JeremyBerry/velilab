# Vélilab

Ce projet est une réplication d'un capteur de distance des dépassements pour vélo, [réalisé par Ian Walker](https://www.drianwalker.com/overtaking/buildasensor.html).

Plus d'information à venir, en attendant quelques détails sont disponibles sur : https://laboratoire-sauvage.fr/?CapteurVelo.

---

This project is a replication of an overtaking sensor for bicycles, [made by Ian Walker](https://www.drianwalker.com/overtaking/buildasensor.html).

More information to come, in the meantime a few details are available at: https://laboratoire-sauvage.fr/?CapteurVelo (in French, but your can use [DeepL](https://www.deepl.com/translator) to translate).
