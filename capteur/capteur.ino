#include <Arduino.h> // Pour le fonctionnement général.
#include <RTClib.h>  // Pour l'horloge embarquée.
// Pour les classes "File" (écrire des fichiers) et "SD" (pouvoir utiliser une carte SD) : https://github.com/arduino-libraries/SD/blob/master/src/.
#include <SD.h>

#define sensorPin      2 // Broche qui recevra les mesures du capteur de distance.
#define greenLEDPin    3 // Broche qui commandera la LED verte.
#define redLEDPin      4 // Broche qui commandera la LED rouge.
#define buttonPin      6 // Broche du bouton poussoir.
// Broche réservée à la carte SD, voir : https://learn.adafruit.com/adafruit-data-logger-shield/using-the-sd-card.
#define SDPin         10

// Horloge interne.
// Inspiration pour le code : https://learn.adafruit.com/adafruit-pcf8523-real-time-clock/rtc-with-arduino.
RTC_PCF8523 rtc;

// Nom du fichier dans lequel ajouter les mesures (il est sur la carte SD).
const String log_file_name = "records.txt";

// ----------------------------------------------------------------------
// VARIABLES POUR EXECUTER A INTERVALLES REGULIERS PLUSIEURS INSTRUCTIONS
// ----------------------------------------------------------------------

/***
   * Ces variables servent de chronomètre pour le temps écoulé (en ms) depuis un évènement passé, jusqu'à l'instant présent.
   * Cette façon de faire permet d'éviter l'utilisation de la fonction "delay()" qui met en pause tout le système, ce qui est problématique
     quand il y a plusieurs tâches à faire en même temps.
   * Une solution consiste à utiliser la fonction "millis()", à la place de "delay()". Ainsi, si l'on souhaite exécuter une tâche toutes les
     n ms, alors on définit un chronomètre grâce à "millis()". Voici ci-dessous comment procéder.
   * Nous nous sommes inspirés pour cette partie de : https://www.aranacorp.com/fr/utilisation-de-la-fonction-millis-de-lide-arduino/.
***/

// C'est le temps actuel (nombre de ms depuis l'allumage du système).
unsigned long ms_from_start_to_now;
// "ms_from_start_to_now - ms_from_previous_read_button" représentera le nombre de ms depuis le dernier changement d'état du bouton poussoir (quand il a été pressé ou quand il a été mis au repos).
unsigned long ms_from_previous_read_button      = 0;
// Même idée, mais pour savoir le temps écoulé depuis la dernière lecture de l'état de la LED qui sert à signaler que le système est prêt à enregistrer.
unsigned long ms_from_previous_read_ready_LED   = 0;
// Même idée, mais pour savoir le temps écoulé depuis le dernier affichage d'un texte dans le moniteur (sur l'ordinateur : sert pour le débogage).
unsigned long ms_from_previous_Serial_print     = 0;
// Même idée, mais pour savoir le temps écoulé depuis la dernière écriture dans un fichier, sur la carte SD.
unsigned long ms_from_previous_print_on_SD_card = 0;

// Choix des intervalles réguliers pour les différentes tâches.
// Combient de temps (en ms) appuyer sur le bouton pour lancer ou arrêter l'enregistrement des mesures du capteur.
unsigned long interval_button              = 1000;
// Choix du nombre de ms entre chaque enregistrement de mesure sur la carte SD.
unsigned long interval_writing_in_log_file = 1000;
// Afficher les messages de débogage tous les combien de ms dans le moniteur de l'IDE Arduino sur l'ordinateur.
unsigned long interval_Serial_print        = 1000;

// ---------------------------------------------------------------
// COMMENT SAVOIR SI L'UTILISATEUR VEUT LANCER UN ENREGISTREMENT ?
// ---------------------------------------------------------------

/***
   * Pour faciliter le traitement de données une fois les données récoltées, nous avons prévus que le cycliste appuie sur
     le bouton poussoir (placé quelque part sur le guidon) pour signaler qu'il se fait dépasser. Cela permettra de reconnaître
     les dépassements par rapport aux faux positifs.
   * Du coup, pour lancer l'enregistrement et pour l'arrêter, nous avons choisis que le signal à envoyer au système sera un appui
     prolongé sur le bouton poussoir (par exemple pendant 1 s). Pour détecter cela, il nous faut savoir depuis quand on a appuyé
     sur le bouton, ce sera avec "ms_from_start_to_now - ms_from_previous_read_button", mais si l'utilisateur appuie trop longtemps sans s'en
     rendre compte, le système va comprendre qu'il faut refaire "basculer" ("toggle" dans le nom des variables) le statut de l'enregistrement
     (c'est-à-dire que si l'on enregistre, on doit arrêter l'enregistrement et vice versa).
      * Pour empêcher ce comportement, on va indiquer que l'on autorise une bascule de l'état de l'enregistrement si et seulement si l'utilisateur
        a appuyé assez longtemps ET que l'on ne vient pas de changer d'état. Cela nous oblige à introduire deux nouvelles variables :
         * "allow_toggle"
         * "toggle_done"
      * Pour comprendre le code plus bas, attention : on sait qu'on vient de changer d'état quand l'état change ET que le bouton poussoir
        est toujours appuyé sans avoir été relâché (remis à l'état de repos).
***/

bool allow_toggle        = false; // Si le changement d'état (pour l'enregistrement) est autorisé.
bool toggle_done         = false; // Si un changement d'état vient d'être fait.
bool overtaking_signaled = false; // Si l'utilisateur vient de signaler un dépassement (de lui par un véhicule).

// ---------------------------------
// LED POUR SIGNALER ETAT DU CAPTEUR
// ---------------------------------

// On a deux LED incluses dans la carte Adafruit : une verte et une rouge.
// Cela nous permet de définir des signaux lumineux à l'utilisateur pour signaler différents états du système
// (enregistrement ou non, s'il est prêt ou non, s'il y a eu une erreur ou non, ...).

// Permet de stocker l'état de la LED verte incluse dans la carte Adafruit ("LOW" pour éteinte et "HIGH" sinon).
bool green_LED_state = LOW;
bool red_LED_state   = HIGH;
// Permet de choisir sa LED pour signaler que l'on est prêt (même si c'est une LED rajoutée plus tard, qui n'est pas sur l'Adafruit).
bool ready_LED_state = LOW;

// Création d'un signal lumineux pour dire que le système fonctionne et est prêt à lancer les enregistrements.
unsigned long interval_ready_signal_on        = 500; // Laisser allumer la LED choisie pour le signalement...
unsigned long interval_ready_signal_off       = 200; // ... puis laisser cette LED éteinte tant de temps, avant de l'allumer à nouveau.

// Variables pour stocker un état du système.
bool problem   = true;  // S'il y a eu un problème ou non.
bool recording = false; // Si l'on enregistre ou non.

// --------------------
// CRASH COURSE ARDUINO
// --------------------

/*** Notions rapides d'Arduino (pour les personnes, qui comme moi en commençant ce projet, ne connaissaient rien sur Arduino) :
      * La programmation en Arduino est basée sur un sous-ensemble des langages de programmation C et C++ (par exemple, il n'y a pas d'"exceptions", c'est-à-dire d'instructions pour gérer des erreurs qui font planter le système, car cela demande trop de mémoire et que sur un système avec trop peu de mémoire, comme un Arduino, les "exceptions" sont un luxe que l'on ne peut pas se permettre. Pas de souci, en programmant assez bien, on peut éviter pas mal d'erreurs).
      * La structure d'un programme Arduino est composée de deux "fonctions" (voir définition ci-dessous) : "setup" et "loop", qui sont deux fonctions spéciales d'Arduino.
         * Pour "setup", voir : https://arduinofactory.fr/langage-arduino-void-setup/.
         * Pour "loop", voir : https://arduinofactory.fr/langage-arduino-void-loop/.
   * Notions rapides de programmation :
      * "void setup() { ... }" signifie que l'on va entrer dans une "fonction", qui est en bref un bout de code ré-utilisable, que l'on peut appeler ailleurs dans le code.
      * L'intérêt d'une fonction est d'écrire le code une seule fois, et que l'on peut l'appeler autant de fois que nécessaire par la suite, sans avoir à recopier le code de la fonction. On économise ainsi de la quantité de code et on facilite sa mise à jour, car il n'y a besoin de changer que le code de la fonction pour qu'il se propage dans tout le programme automatiquement.
      * Voici comment définir une fonction :
         * Une fonction se définit en deux temps : il y a son "entête" et son "corps".
            * L'entête est la première ligne, jusqu'au premier crochet "{".
            * Le corps est le code de la fonction, il se situe entre les deux crochets "{" et "}".
      * Voici comment lire l'entête "void setup()" de la fonction "setup" :
         * Elle s'appelle "setup".
         * Elle ne prend aucun paramètre, c'est-à-dire aucune information lors de son appel. Cela s'indique par des parenthèses vides : "()".
         * Le "void" devant son nom désigne le "type" de données que la fonction doit renvoyer à l'issue de son exécution. Ainsi, "void" signifie que la fonction ne renvoie rien. Si elle renvoie un entier (ce qui se note "int" pour "integer" en anglais), la définition devra être "int setup()" et il devra y avoir dans le corps de la fonction un ordre de renvoie sur un entier, ce qui se note "return un_entier;".
***/

// Configuration du système.
void setup() {

  // Initialisation de la communication par le port série (sert pour communiquer avec l'ordinateur, on lui indique avec quel débit échanger des données).
  Serial.begin(9600);

  // --------------------
  // REGLAGE DE L'HORLOGE
  // --------------------

  // On vérifie que l'on puisse se connecter à la puce PCF8523 qui gère l'horloge.
  // Voir pour notre cas : // https://github.com/adafruit/RTClib/blob/5dde49bf452e55e9f8c0052295f927bca4ab6c69/src/RTC_PCF8523.cpp#L20.
  if(!rtc.begin()) {
    Serial.println("Il ne semble pas y avoir d'horloge interne utilisable (module RTC introuvable/indisponible). Il faut régler ce souci et me redémarrer.");
    Serial.flush();
    while(1) delay(10);
  }

  // Le code de l'exemple (https://learn.adafruit.com/adafruit-pcf8523-real-time-clock/rtc-with-arduino) pour l'initialisation donne :
  if (!rtc.initialized() || rtc.lostPower()) {
    // Ce qui signifie : si l'horloge n'a pas été "initialisée" ou qu'il y a eu une coupure de courant (pile plate vide ou qui a été retiré par exemple), alors on règle l'horloge.
    Serial.println("L'horloge interne n'est PAS initialisée, je la règle pour vous ;-)");
    // La date sera réglée sur celle de l'ordinateur au moment de la compilation.
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  // La macro F sert apparemment à stocker la date dans la mémoire Flash plutôt que la RAM, pour l'économiser. Voir :
  //    * https://github.com/SpellFoundry/PCF8523/blob/7f4d5949845d3705e4d4724888f2a30445a1408b/PCF8523.cpp#L169
  //    * https://arduinogetstarted.com/fr/reference/arduino-progmem
  }

  // -------------------------------------
  // REGLAGE DES LED ET DU BOUTON POUSSOIR
  // -------------------------------------

  // On règle les broches des LED pour qu'elles soient des sorties (pour que les broches donnent un signal quand on le demande).
  pinMode(LED_BUILTIN, OUTPUT); // LED intégrée sur la carte Arduino (qui sert aussi pour la LED de la carte Adafruit qui est synchronisée elle).
  pinMode(redLEDPin, OUTPUT);   // LED rouge intégrée à la carte Adafruit.
  pinMode(greenLEDPin, OUTPUT); // LED verte intégrée à la carte Adafruit.
  // On indique que la broche 2 sera une entrée (INPUT) qui doit activer sa résistance de tirage (PULLUP), voir : https://fr.wikipedia.org/wiki/Résistance_de_rappel.
  // Voir : https://docs.arduino.cc/built-in-examples/digital/InputPullupSerial.
  //    * "When your button is not pressed, the internal pull-up resistor connects to 5 volts. This causes the Arduino to report "1" or HIGH."
  //    * "When the button is pressed, the Arduino pin is pulled to ground, causing the Arduino report a "0", or LOW."
  pinMode(buttonPin, INPUT_PULLUP);

  // --------
  // CARTE SD
  // --------

  Serial.print("Initialisation de la carte SD... ");

  // On vérifie si la carte SD est disponible.
  if(!SD.begin(SDPin)) {
    problem = true;
    Serial.println("Echec : pas de carte ou carte illisible. Il faut régler ce souci et me redémarrer.");
  } else {
    Serial.println("Carte SD disponible !");
    problem = false;
  }

  Serial.println("En attente des instructions via le bouton poussoir...");
}

void loop() {

  // On récupère la date de la boucle en cours, à la seconde près.
  // Nous n'avons pas besoin de descendre à la milliseconde (la fonction "millis()" permet de le faire pour les intervalles, mais pour les mesures de dépassement, cette précision n'est pas pertinente).
  DateTime rtc_now = rtc.now();
  // Texte commun à chaque début de ligne pour les entrées dans le fichier des engistrements (il y a plusieurs types d'enregistrement : ceux pour les dépassements, ceux pour les mesures et ceux pour les changement d'état).
  // Ce texte contient donc l'horodatage au format Unix ("rtc_now.unixtime()") et la date dans un format lisible par un humain ("date_time_to_str(rtc_now)").
  String time_str = String(rtc_now.unixtime()) + "-" + date_time_to_str(rtc_now) + "-";

  // Pour rappel, la fonction "millis()" ne prend aucun paramètre et renvoie une valeur qui représente le nombre de millisecondes écoulées depuis la mise en tension de l'Arduino. Elle est très pratique pour repérer depuis combien de temps on appuie sur le bouton, pour savoir s'il faut lancer ou non l'enregistrement notamment.
  // Voir : https://www.aranacorp.com/fr/utilisation-de-la-fonction-millis-de-lide-arduino/.
  ms_from_start_to_now = millis();

  // ------------------------
  // SIGNALER ETAT DU SYSTEME
  // ------------------------

  // Signal lumineux pour indiquer que le système détecte bien que l'on appuie sur le bouton poussoir : la LED verte s'allume si c'est le cas.
  // S'il n'y a pas de signal lumineux en appuyant sur le bouton, il y a peut-être un problème de contact avec la LED ou avec le bouton (ou l'un de ces composants a été endommagé).
  if(!digitalRead(buttonPin))
    digitalWrite(greenLEDPin, HIGH);
  else
    digitalWrite(greenLEDPin, LOW);
  
  // On ne peut démarrer les enregistrements que s'il n'y a eu aucun problème jusqu'ici (donc horloge interne et carte SD doivent être disponibles).
  if(problem) {
    Serial.println("Un problème est survenu dans 'loop()' !");
    // On peut imaginer un signal lumineux ici.
  } else {
    // Si l'enregistrement n'a pas commencé, on fait clignoter la LED choisie pour signaler que le système est prêt à enregistrer.
    if(!recording)
      ready_signal(redLEDPin);
    
    // -------------------------------------------------------------------------
    // LECTURE DE L'ETAT DU BOUTON POUSSOIR ET DECLENCHEMENT DES ENREGISTREMENTS
    // -------------------------------------------------------------------------

    // Il y a trois variables, donc 2^3 = 8 cas possibles qui nous permettent de gérer tous les cas d'usage (lancer enregistrement sans faire un signalement, ...).
    // Voir le diagramme des étapes (et le tableau des 8 cas possibles).
    if(!digitalRead(buttonPin)) { // Quand on appuie...
      // ETAPE 1
      if((ms_from_start_to_now - ms_from_previous_read_button) > interval_button) { // ... si on appuie assez longtemps...
        ms_from_previous_read_button = ms_from_start_to_now;
        if(!toggle_done) // ... et que l'on n'a pas fait de bascule d'état pour le moment...
          allow_toggle = true; // ... alors autoriser la bascule d'état.
        else // Sinon, la bascule a été faite...
          allow_toggle = false; // ... et à ce moment-là, on n'a pas le droit de re-changer l'état des enregistrements (il faut attendre que l'utilisateur relâche le bouton poussoir).
      }
      // ETAPE 2
      if(allow_toggle && !toggle_done) { // Si l'on a le droit de faire la bascule et qu'elle n'a pas encore été faite...
        ms_from_previous_read_button = ms_from_start_to_now;
        recording = !recording; // On fait la bascule (on dit ici au programme d'inverser la valeur de "recording" : si c'était à "true", ça passe à "false" et vice versa).

        // On signale le moment du changement d'état sur la carte SD (début et fin de l'enregistrement).
        String rec_str = time_str + (recording ? "START" : "END");
        // Puis on ajoute la mesure dans le fichier.
        save_to_log(rec_str, true);

        // Signal lumineux pour indiquer que l'enregistrement est en cours : la LED choisie s'allume si c'est le cas (éviter d'utiliser la LED interne d'Arduino, qui s'allume automatiquement, en tout cas pour nos essais, quand une écriture a lieu sur la carte SD).
        if(recording)
          digitalWrite(redLEDPin, HIGH);
        else
          digitalWrite(redLEDPin, LOW);
          
        // ETAPE 3
        // On vient de faire une bascule, on n'a donc plus le droit d'en faire une nouvelle (tant que l'utilisateur n'a pas relâché le bouton et appuyé à nouveau dessus assez longtemps).
        allow_toggle = false;
        // On fait retenir au système qu'une bascule de l'état des enregistrements vient d'être faite.
        toggle_done = true;
        
        // Au cas où l'utilisateur appuierait trop longtemps sur le bouton après le début de l'enregistrement sans avoir relâché le bouton, on précise au système de ne pas compter cet appui sur le bouton comme un dépassement. Il faut relâcher le bouton une fois d'abord.
        overtaking_signaled = false;
      }
    } else { // ETAPE 4
      ms_from_previous_read_button = ms_from_start_to_now;
      if(toggle_done) // Si l'on vient de faire la bascule, alors on indique que dès la prochaine itération de la boucle "loop()" qu'aucune bascule n'a été faite (pour la nouvelle itération : l'état de la bascule décrit l'état pour la boucle en cours).
        toggle_done = false;
    }

    // Si l'enregistrement sur la carte SD est activé.
    if(recording) {

      // Si le cycliste appuie sur le bouton pour signaler un dépassement, on inclut la balise de dépassement du cycliste dans le fichier d'enregistrement.
      // Il faut que ce soit un appui qui ne soit pas un appui pour activer ou désactiver l'enregistrement.
      if(!allow_toggle && !toggle_done && !digitalRead(buttonPin)) {
        overtaking_signaled = true; // On signale au système que l'appui est un appui pour un dépassement spécifiquement.
      }

      // Pour s'assurer que l'on enregistre qu'une seule ligne dans le fichier quand on appuie pour le dépassement
      // (comme le système est trop rapide pour l'humain, même en appuyant vite, il aura le temps d'enregistrer plusieurs lignes,
      // les conditions ici sont pour éviter d'avoir plusieurs lignes, en enregistrant notamment uniquement quand l'utilisateur relâche le bouton,
      // qui est lui un moment unique pour la carte).
      if(overtaking_signaled && digitalRead(buttonPin)) {
        // On construit la ligne pour le fichier, pour indiquer un dépassement du cycliste, et on l'enregistre.
        String rec_str = time_str + "OVERTAKING";
        save_to_log(rec_str, true);

        // Le signalement du dépassement est terminé, on peut en signaler un autre avec un nouvel appuie.
        overtaking_signaled = false;
      }

      // On fait les mesures à un intervalle régulier.
      if((ms_from_start_to_now - ms_from_previous_print_on_SD_card) > interval_writing_in_log_file) {
        ms_from_previous_print_on_SD_card = ms_from_start_to_now;

        // On récupère la mesure à enregistrer sur la carte SD.
        String rec_str = time_str + "D-" + String(distance_reading()) + "-cm";

        // Puis on ajoute la mesure dans le fichier.
        save_to_log(rec_str, true);
      }
    }
  }

  // POUR DES TESTS AVEC LE PC
  // Pour pouvoir afficher ce que l'on veut dans le moniteur en parallèle des autres actions : pour tester le fonctionnement, au besoin.
  /*
  if((ms_from_start_to_now - ms_from_previous_Serial_print) > interval_Serial_print) {
    ms_from_previous_Serial_print = ms_from_start_to_now;
    Serial.print("recording = " + String(recording));
    if(recording)
      Serial.print(" - distance = " + String(distance_reading()) + " cm");
    Serial.println();
  }
  */
}

// Conversion d'une date en une chaîne de caractères.
String date_time_to_str(DateTime &rtc_now) { // On passe une référence en paramètre pour optimiser la mémoire.
  char rtc_now_str[] = ""; // On ne peut pas juste noter "char rtc_nowStr[];", il faut initialiser avec une chaîne vide.
  sprintf(rtc_now_str, "%4d/%02d/%02d %02d:%02d:%02d", rtc_now.year(), rtc_now.month(), rtc_now.day(), rtc_now.hour(), rtc_now.minute(), rtc_now.second());
  // La chaîne "%4d/%02d/%02d %02d:%02d:%02d" définie le formatage à respecter en utilisant (dans l'ordre) les arguments qui suivent.
  // L'expression "%4d" signifie que l'on va devoir afficher 4 "digits" (4 chiffres).
  // On aura donc : "AAAA/MM/JJ HH:mm:ss" (exemple : "2023/03/14 12:22:21").

  return String(rtc_now_str);
}

// Enregistrement de la chaîne "str" dans le fichier "log_file_name" sur la carte SD.
// Si "debug" est à "true", on affiche "str" dans le moniteur de l'IDE Arduino sur l'ordinateur (comme aide pour comprendre ce qu'il se passe).
void save_to_log(String str, bool debug) {
  // On se prépare à écrire dans un fichier sur la carte SD.
  // Les nouvelles mesures sont ajoutées comme de nouvelles lignes dans le fichier, ainsi aucune donnée n'est effacée entre les sessions d'enregistrement.
  // Pour effacer des données, il faut retirer la carte SD de l'Adafruit et la mettre sur son ordinateur, pour la vider.
  File log_file = SD.open(log_file_name, FILE_WRITE);
  if(log_file) { // Il n'y a pas besoin d'utiliser un signal lumineux pour indiquer qu'un enregistrement est en cours : la LED interne clignote normalement automatiquement ;-)
    log_file.println(str);
    // Affiche dans le moniteur (de l'IDE Arduino sur l'ordinateur) si l'on veut savoir ce que le programme écrit sur la carte SD en temps réel.
    if(debug)
      Serial.println(str);
    log_file.close();
  } else // S'il y a eu un problème, on l'affiche dans le moniteur de l'IDE Arduino.
    Serial.println("Error opening \"" + log_file_name);
  
}

// Récupération de la distance lue par le capteur.
// Voir au besoin : http://www.arduino.cc/playground/Main/MaxSonar.
long distance_reading() {
  long reading; // Lecture brute du capteur.
  long cm;      // Résultat en cm, après conversion.

  reading = pulseIn(sensorPin, HIGH); // On lit la broche sur laquelle le capteur est branché.
  
  cm = reading / 58; // Facteur de conversion : 1 cm est parcouru en 58 ms.
  
  return(cm);
}

// Pour signaler quand la carte est inoccupée, mais prête à enregistrer : on fait clignoter la LED verte
// Inspiration : https://www.aranacorp.com/fr/utilisation-de-la-fonction-millis-de-lide-arduino/.
void ready_signal(int ready_LED_pin) {
  unsigned long interval;

  if(ready_LED_state) { // Si la LED doit être allumée...
    interval = interval_ready_signal_on; // ... alors on va lui demander de le rester "interval_ready_signal_on" ms.
  } else {
    interval = interval_ready_signal_off;
  }

  if((ms_from_start_to_now - ms_from_previous_read_ready_LED) > interval) {
    ms_from_previous_read_ready_LED = ms_from_start_to_now;
    ready_LED_state = !ready_LED_state;
    digitalWrite(ready_LED_pin, ready_LED_state);
  }
}